import numpy as np
import scipy.interpolate as interp

rmdir = '/Users/jackelvinpoole/DES/cats/y3/redmagic/combined_sample_6.4.22/'
fid_nz_file = rmdir + 'nz_combined.txt'

nzdata = np.loadtxt(fid_nz_file,unpack=True)
z = nzdata[0]
nz = nzdata[1:]

def stretch_shift(z,nz,stretch,deltaz,z_eval):
	"""
	n(z) = n_fid(stretch*(z - zmean) + zmean + deltaz) I think
	"""
	zmean = np.average(z,weights=nz)
	return interp.interp1d(stretch*(z-zmean)+zmean+deltaz, nz, fill_value=0.,bounds_error=False)(z_eval)/stretch

bf = [
	[0.984343434343, 0.00161616161616],
	[0.970202020202, 0.00212121212121],
	[1.06919191919,  0.00338383838384],
	[0.90303030303,  0.00111111111111],
	[1.0, 0.0],
]

nz_bf = []
for ibin in xrange(5):
	nz_bf.append(stretch_shift(z,nz[ibin],bf[ibin][0],bf[ibin][1],z))

np.savetxt('nzlens_bf.txt',np.transpose(np.vstack((z,nz_bf))))

for stretch1 in [0.8,0.9,1.0,1.1,1.2]:
	values = [
	[stretch1,0.0],
	[stretch1,0.0],
	[stretch1,0.0],
	[stretch1,0.0],
	[stretch1,0.0],
	]

	nz_grid = []
	for ibin in xrange(5):
		nz_grid.append(stretch_shift(z,nz[ibin],values[ibin][0],values[ibin][1],z))

	np.savetxt('nzlens_grid_stretch{0}.txt'.format(stretch1),np.transpose(np.vstack((z,nz_grid))))



