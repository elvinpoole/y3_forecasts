
params_template = """
%include params_lensnz.ini

[DEFAULT]
2PT_DATA_SETS = {DATA_SETS}
NZ_NAME = {NZ_NAME}
NZ_MODEL = {NZ_MODEL}

[runtime]
sampler = multinest

[multinest]
multinest_outfile_root=
"""


adj_options = """
[2pt_like]
cut_wtheta = 1,3 1,4 1,5 2,4 2,5 3,5
"""

all_options = """
"""

auto_options = """
[2pt_like]
cut_wtheta = 1,2 1,3 1,4 1,5 2,3 2,4 2,5 3,4 3,5 4,5
"""

fixed_modules = """
"""
mean_modules = """
"""
width_modules = """
"""
mean_width_modules = """
[pipeline]
;stretch then shift!
modules =  cholesky_nz consistency bbn_consistancy camb halofit growth extrapolate fits_nz lens_nz lens_photoz_width lens_photoz_bias source_photoz_bias unbiased_galaxies bias_neutrinos  multiply_pk IA ia_z_field pk_to_cl bin_bias add_intrinsic shear_m_bias 2pt_gal 2pt_gal_shear 2pt_shear 2pt_like

[cholesky_nz]
file = cosmosis-des-library/photoz/cholesky_nz.py
section = lens_photoz_errors
var_bias  		 =  4.087731608931888860e-06  2.622586793871518070e-06  7.316499682655811521e-06  7.642938748132916063e-06  3.299437929556322223e-05
var_width 		 =  5.401584719295200948e-04  4.741247757347676842e-04  7.530036326299974605e-04  4.789989717096466764e-04  6.449166728487224637e-03
covar_bias_width = -1.794690905286923821e-06 -1.676410259287194950e-05  5.321269606376181660e-05  8.488426233851499871e-06  1.527218528901829614e-04
mean_bias        =  4.82843314e-03 -1.01852975e-03 1.97770942e-03 -6.18928780e-04 3.95984552e-02
mean_width       =  9.84674622e-01  9.39380123e-01 1.11125581e+00  9.11220053e-01 1.27906219e+00
"""

launch_template = """
#PBS -N {run_name}
#PBS -l walltime=160:00:00
#PBS -l nodes=1:ppn=40
#PBS -j oe
#PBS -m abe
#PBS -A PCON0003

cd $PBS_O_WORKDIR

source ~/cosmosis/config/setup-cosmosis
mpiexec -n 40 cosmosis --mpi {params_filename}
"""

run_table = [
	#'dataset', 'data n(z)' (which fits file to use), 'model n(z)' (rm is always the tempalte) 
	['d2', 'rm', 'fixed'],
	#['d2', 'rm', 'mean'],
	#['d2', 'rm', 'width'],
	#['d2', 'rm', 'mean-width'],
	['d2', 'eboss', 'fixed'],
	['d2', 'eboss', 'mean'],
	['d2', 'eboss', 'width'],
	['d2', 'eboss', 'mean-width'],
	['d3', 'rm', 'fixed'],
	#['d3', 'rm', 'mean'],
	#['d3', 'rm', 'width'],
	#['d3', 'rm', 'mean-width'],
	['d3', 'eboss', 'fixed'],
	['d3', 'eboss', 'mean'],
	['d3', 'eboss', 'width'],
	['d3', 'eboss', 'mean-width'],
	#['d2-adj', 'rm', 'fixed'],
	#['d2-adj', 'rm', 'mean'],
	#['d2-adj', 'rm', 'width'],
	#['d2-adj', 'rm', 'mean-width'],
	['d2-adj', 'eboss', 'fixed'],
	['d2-adj', 'eboss', 'mean'],
	['d2-adj', 'eboss', 'width'],
	['d2-adj', 'eboss', 'mean-width'],
	#['d3-adj', 'rm', 'fixed'],
	#['d3-adj', 'rm', 'mean'],
	#['d3-adj', 'rm', 'width'],
	#['d3-adj', 'rm', 'mean-width'],
	['d3-adj', 'eboss', 'fixed'],
	['d3-adj', 'eboss', 'mean'],
	['d3-adj', 'eboss', 'width'],
	['d3-adj', 'eboss', 'mean-width'],
]

for i in xrange(len(run_table)):
	data = run_table[i][0]
	nz_name = run_table[i][1]
	nz_model = run_table[i][2]

	run_name = '{data}-{nz_name}_l-{nz_model}'.format(data=data, nz_name=nz_name, nz_model=nz_model)
	params_filename =  'params_{run_name}.ini'.format(run_name=run_name)
	launch_filename =  'launch_{run_name}.sub'.format(run_name=run_name)

	if 'd2' in data:
		#data_sets = 'gammat wtheta'
		data_sets = 'galaxy_xi galaxy_shear_xi'
	elif 'd3' in data:
		#data_sets = 'xip xim gammat wtheta'
		data_sets = 'galaxy_xi galaxy_shear_xi shear_xi_plus shear_xi_minus'
	else:
		raise RuntimeError('data set not recognised')

	if 'adj' in data:
		extra_lines = adj_options
	else:
		extra_lines = auto_options

	if nz_model == 'fixed':
		extra_lines = extra_lines + fixed_modules
	elif nz_model == 'mean':
		extra_lines = extra_lines + mean_modules
	elif nz_model == 'width':
		extra_lines = extra_lines + width_modules
	elif nz_model == 'mean-width':
		extra_lines = extra_lines + mean_width_modules

	f = open(params_filename,'w')
	f.write( params_template.format(DATA_SETS=data_sets, NZ_NAME=nz_name, NZ_MODEL=nz_model) + extra_lines )
	f.close()

	f = open(launch_filename, 'w')
	f.write( launch_template.format(run_name=run_name, params_filename=params_filename) )
	f.close()





