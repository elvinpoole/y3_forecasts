import numpy as np 

#sc = (wtheta, gammat)
sc = (8, 8)

#y3 mag lim
kernel_peaks_file = 'test_save2pt_nomag/galaxy_cl/values.txt'

#y1 redmagic
#kernel_peaks_file = 'test_sc_y1/test_save2pt_maglim_nomag/galaxy_cl/values.txt'

nbins = 6

s = open(kernel_peaks_file,'r').read()

am_lines = [line for line in s.split('\n') if 'arcmin_per_mpch' in line]

for ibin in xrange(1,nbins+1):
	matching_lines = [line for line in am_lines if '{0}_{0}'.format(ibin) in line]
	assert len(matching_lines) == 1
	arcmin_per_mpch = float(matching_lines[0].split('=')[-1])


	print 'angle_range_galaxy_shear_xi_{0}_1 = {1} 250.0'.format(ibin, sc[1]*arcmin_per_mpch)
	print 'angle_range_galaxy_shear_xi_{0}_2 = {1} 250.0'.format(ibin, sc[1]*arcmin_per_mpch)
	print 'angle_range_galaxy_shear_xi_{0}_3 = {1} 250.0'.format(ibin, sc[1]*arcmin_per_mpch)
	print 'angle_range_galaxy_shear_xi_{0}_4 = {1} 250.0'.format(ibin, sc[1]*arcmin_per_mpch)

print ''

for ibin in xrange(1,nbins+1):
	matching_lines = [line for line in am_lines if '{0}_{0}'.format(ibin) in line]
	assert len(matching_lines) == 1
	arcmin_per_mpch = float(matching_lines[0].split('=')[-1])


	print 'angle_range_galaxy_xi_{0}_{0} = {1} 250.0'.format(ibin, sc[0]*arcmin_per_mpch)




