import numpy as np
import pylab as plt 
import lsssys
import scipy.interpolate as interp

rmdir = '/Users/jackelvinpoole/DES/cats/y3/redmagic/combined_sample_6.4.22/'
nz_file = rmdir + 'nz_combined.txt'
rm_nz_table = np.loadtxt(nz_file,unpack=True)
np.savetxt('nz_rm.txt', np.transpose(rm_nz_table))

coeff_file = '../nz/fit_template/nz_width_mean_coeff.txt'
coeff_list = np.loadtxt(coeff_file) 

nz_bf_table = []
nz_bf_table.append(rm_nz_table[0])
for ibin in xrange(len(coeff_list)):

	z_theory = rm_nz_table[0]
	nz_theory = rm_nz_table[ibin+1]


	def stretch_shift_1(z_eval,stretch,deltaz):
		"""
		n(z) = n_fid(stretch*(z - zmean) + zmean + deltaz) I think
		"""
		zmean = np.average(z_theory,weights=nz_theory)
		return interp.interp1d(stretch*(z_theory-zmean)+zmean+deltaz, nz_theory, fill_value=0.,bounds_error=False)(z_eval)/stretch


	stretch, deltaz = coeff_list[ibin]

	nz_bf = stretch_shift_1(z_theory,stretch,deltaz)

	nz_bf_table.append(nz_bf)



np.savetxt('nz_xcorr_eboss_bf_width_mean.txt', np.transpose(nz_bf_table))

[plt.plot(rm_nz_table[0],rm_nz_table[ibin+1], color='r') for ibin in xrange(5)]
[plt.plot(nz_bf_table[0],nz_bf_table[ibin+1], color='k') for ibin in xrange(5)]
plt.xlim([0.1,1.25])
plt.savefig('nz.png')
plt.close()




