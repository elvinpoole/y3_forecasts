template = """
#PBS -N {run_name}
#PBS -l walltime={walltime}:00:00
#PBS -l nodes=1:ppn={cores}
#PBS -j oe
#PBS -m abe
#PBS -A {project}

cd $PBS_O_WORKDIR

{source}
export DATAFILE={datafile}
mpiexec -n {cores} cosmosis --mpi params_{run_name}.ini 
"""

runs = [
'd3_l_bf',         
'd3_l_stretch0.8', 
'd3_l_stretch1.0', 
'd3_l_stretch1.2',
'd3_l_fixeddelta', 
'd3_l_stretch0.9', 
'd3_l_stretch1.1',
]

for r in runs:
	launchfile = 'launch_{0}.sub'.format(r)
	f = open(launchfile,'w')

	f.write(
		template.format(
		run_name=r, 
		walltime='160',
		cores='28',
		project='PCON0008',
		source='source ~/cosmosis/config/setup-cosmosis',
		datafile='simulated_redmagic_hd3_hl2_sourcesy3_cosmo-ptt_cov-cosmolikeG.fits',
		)
	)
	f.close()
